import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import unityofwork.HsqlUnitOfWork;
import domain.User;
import repositories.HsqlUsersRepository;

public class Main {

	public static void main(String[] args) {
		String url = "jdbc:hsqldb:hsql://localhost/workdb";
		
		try {
		Connection connection = DriverManager.getConnection(url, "SA", null);
		Statement statement = connection.createStatement();
		
		statement.executeQuery("CREATE TABLE IF NOT EXISTS t_sys_users ("
				+ "id int, state varchar(15),"
				+ "login varchar(30),"
				+ "password varchar(30));");
		
		statement.executeQuery("CREATE TABLE IF NOT EXISTS t_sys_enums ("
				+ "id int,"
				+ "state varchar(15),"
				+ "intKey int,"
				+ "stringKey varchar(30),"
				+ "value varchar(50),"
				+ "enumerationName varchar(30));");
		
		User user = new User();
		user.setId(1);
		user.setLogin("Sebastian");
		user.setPassword("zab123");
		
		HsqlUsersRepository repo = new HsqlUsersRepository();
		repo.add(user);
		
		HsqlUnitOfWork uow = new HsqlUnitOfWork(connection);
		uow.savechanges();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}