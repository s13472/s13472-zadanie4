package domain;

import java.util.List;

public class User extends Entity {
	
	private String login;
	private String password;
	private List <UserRoles> userRoles;
	private List <RolesPremissions> rolesPermissions;
	
	
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<UserRoles> getUserRoles() {
		return userRoles;
	}
	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}
	public List<RolesPremissions> getRolesPermissions() {
		return rolesPermissions;
	}
	public void setRolesPermissions(List<RolesPremissions> rolesPermissions) {
		this.rolesPermissions = rolesPermissions;
	}
	
	

}
