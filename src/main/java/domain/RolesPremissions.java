package domain;

public class RolesPremissions extends Entity {
	
	private int roleId;
	private int premissionId;
	
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getPremissionId() {
		return premissionId;
	}
	public void setPremissionId(int premissionId) {
		this.premissionId = premissionId;
	}
	
	

}
