package unityofwork;

import domain.Entity;

public interface UnitOfWorkRepository {

	public void persistAdd (Entity entity);
	public void persistDelete (Entity entity);
	public void persistUpdate (Entity entity);
}
