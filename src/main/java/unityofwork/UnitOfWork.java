package unityofwork;

import domain.Entity;

public interface UnitOfWork {
	
	public void savechanges();
	public void undo();
	public void markAsNew(Entity entity, UnitOfWorkRepository repo);
	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo);
	public void markAsChanged(Entity entity, UnitOfWorkRepository repo);
	

}
