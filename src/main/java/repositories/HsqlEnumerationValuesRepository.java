package repositories;

import java.util.ArrayList;
import java.util.List;

import domain.Entity;
import domain.EntityState;
import domain.EnumerationValue;
import domain.User;
import unityofwork.UnitOfWorkRepository;

public class HsqlEnumerationValuesRepository implements EnumerationValueRepository, UnitOfWorkRepository {

	private DataBase database;		
	
	public HsqlEnumerationValuesRepository (DataBase database) {
	this.database = database;
	}
	
	public Object withId(int Id) {
		for (User user : database.getUser()) { 
			if (user.getId() == Id)
				return user; 
		}
		return null;
	}

	public List <User> allOnPage(PagingInfo page) {
		List <User> user = new ArrayList <User> ();

        for(User u : database.getUser())
            if(user.size() < page.getPageSize())
                user.add(u);

        return user;
	}

	public void add(Object entity) {
		this.database.users.add((User) entity);
		
	}

	public void delete(Object entity) {
		this.database.users.remove((User) entity);
		
	}

	public void modify(Object entity) {
		int index = database.getUser().indexOf(entity);
		if (index >= 0) { 
			database.getUser().get(index).setState(EntityState.Changed); 
		}	
		
	}

	public int count() {
		return database.getUser().size(); 
	}

	public EnumerationValue withName(String name) {
		for (EnumerationValue e : database.getEnumerationValue()) {
			if (e.getEnumerationName() == name) 
				return e;			
		}
		return null;
	}

	public EnumerationValue withIntKey(int key, String name) {
		for (EnumerationValue e : database.getEnumerationValue()) {
			if ((e.getIntKey() == key) && (e.getEnumerationName() == name)) 
				return e;
		}
		
		return null;
	}

	public EnumerationValue withStringKey(String key, String name) {
		for (EnumerationValue e : database.getEnumerationValue()) {
			if ((e.getStringKey() == key) && (e.getEnumerationName() == name)) 
				return e;
		}
		
		return null;
	}

	public void persistAdd(Entity entity) {
		this.database.users.add((User)entity);
		
	}

	public void persistDelete(Entity entity) {
		this.delete(entity);
		
	}

	public void persistUpdate(Entity entity) {
		this.modify(entity);
		
	}

}
